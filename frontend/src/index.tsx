import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import PropertyList from "./containers/PropertyList";
import rootReducer from "./store/reducers";

import './index.scss';

const store = createStore(rootReducer, applyMiddleware(thunk));

ReactDOM.render(
    <Provider store={store}>
        <PropertyList />
    </Provider>,
    document.getElementById("app")
);