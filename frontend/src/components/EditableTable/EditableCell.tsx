import * as React from "react";
import { Form, Input } from "antd";
import { EditableContext } from "./EditableContext";

interface Props {
    editing: boolean;
    dataIndex: string;
    title: string;
    record: any;
}

export default class EditableCell extends React.PureComponent<Props> {
    render() {
        const {
            editing,
            dataIndex,
            title,
            record,
            ...restProps
        } = this.props;

        return (
            <EditableContext.Consumer>
                {(form) => {
                    const { getFieldDecorator } = form;

                    return (
                        <td>
                            {editing ? (
                                <Form.Item style={{ margin: 0 }}>
                                    {getFieldDecorator(dataIndex, {
                                        rules: [{
                                            required: true,
                                            message: `Please Input ${title}!`,
                                        }],
                                        initialValue: record[dataIndex],
                                    })(<Input />)}
                                </Form.Item>
                            ) : restProps.children}
                        </td>
                    );
                }}
            </EditableContext.Consumer>
        );
    }
}