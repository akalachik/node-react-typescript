import * as React from 'react';

export const EditableContext = React.createContext(undefined);
