import * as React from "react";
import { ReactNode } from "react";
import { Table, Popconfirm } from "antd";
import { ColumnProps } from "antd/lib/table";

import EditableRow from "./EditableRow";
import EditableCell from "./EditableCell";

import { EditableContext } from "./EditableContext";

interface Props {
    columns: ColumnProps<any>[];
    data: any[];
    onSave: (row: any) => void;
    onDelete: (id: number) => void;
    expandedRowRender: (row: any) => ReactNode;
}

interface State {
    editingKey: string;
}

export default class EditableTable extends React.PureComponent<Props, State> {
    state = { editingKey: '' };

    columns: ColumnProps<any>[] = [];

    constructor(props) {
        super(props);

        const editableColumns = props.columns.map(
            column => ({ ...column, editable: typeof column.editable === 'undefined' ? true : column.editable })
        );

        this.columns = [...editableColumns,
            {
                width: '20%',
                key: 'actions',
                title: 'Actions',
                dataIndex: 'actions',
                render: this.renderActionsColumn
            }
        ];
    }

    isEditing = record => record.id === this.state.editingKey;

    edit = id => this.setState({ editingKey: id });

    save = (form, id) => {
        form.validateFields((error, row) => {
            if (error) {
                return;
            }

            const index = this.props.data.findIndex(item => id === item.id);

            if (index > -1) {
                const item = this.props.data[index];

                const update = { ...row, id: item.id, address: item.address };

                this.props.onSave(update);

                this.setState({ editingKey: '' });
            }
        });
    };

    cancel = () => this.setState({ editingKey: '' });

    renderActionsColumn = (text, record) => {
        const editable = this.isEditing(record);

        return (
            <div>
                {editable ? (
                    <span>
                      <EditableContext.Consumer>
                        {form => (
                            <a
                                onClick={() => this.save(form, record.id)}
                            >
                                Save
                            </a>
                        )}
                      </EditableContext.Consumer>
                      <Popconfirm
                          title="Sure to cancel?"
                          onConfirm={this.cancel}
                      >
                        <a>Cancel</a>
                      </Popconfirm>
                    </span>
                ) : (
                    <>
                    <a onClick={() => this.edit(record.id)}>Edit</a>
                    <Popconfirm
                        title="Sure to delete?"
                        onConfirm={() => this.props.onDelete(record.id)}
                    >
                        <a>Delete</a>
                    </Popconfirm>
                    </>
                )}
            </div>
        );
    }

    render() {
        const components = {
            body: {
                row: EditableRow,
                cell: EditableCell,
            },
        };

        const columns = this.columns.map((col: any) => {
            if (!col.editable) {
                return col;
            }
            return {
                ...col,
                onCell: record => ({
                    record,
                    inputType: 'text',
                    dataIndex: col.dataIndex,
                    title: col.title,
                    editing: this.isEditing(record),
                }),
            };
        });

        return (
            <Table
                components={components}
                bordered={true}
                dataSource={this.props.data}
                columns={columns}
                rowKey={'id'}
                pagination={false}
                expandedRowRender={this.props.expandedRowRender}
            />
        );
    }
}