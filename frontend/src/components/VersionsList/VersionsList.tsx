import * as React from "react";
import { Table } from "antd";
import { PropertyVersion } from "../../store/property/interfaces";

interface Props {
    versions: PropertyVersion[];
}

export default class VersionsList extends React.PureComponent<Props> {
    columns = [
        {
            title: 'Host',
            dataIndex: 'owner',
            key: 'owner',
        }, {
            title: 'Number Of Bedrooms',
            dataIndex: 'numberOfBedrooms',
            key: 'numberOfBedrooms',
        }, {
            title: 'Number Of Bathrooms',
            dataIndex: 'numberOfBathrooms',
            key: 'numberOfBathrooms',
        }, {
            title: 'Airbnb ID',
            dataIndex: 'airbnbId',
            key: 'airbnbId',
        }, {
            title: 'Income Generated',
            dataIndex: 'incomeGenerated',
            key: 'incomeGenerated',
        }, {
            title: 'Changed At',
            dataIndex: 'createdAt',
            key: 'createdAt',
        }
    ];

    render() {
        return <Table
            bordered={true}
            dataSource={this.props.versions}
            columns={this.columns}
            rowKey={'id'}
            pagination={false}
        />;
    }
}