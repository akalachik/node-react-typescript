import * as React from "react";

import { connect } from "react-redux";
import { Property } from "../store/property/interfaces";
import { getData, editData, deleteData } from "../store/property/actions";
import { AppState } from "../store/interfaces";
import EditableTable from "../components/EditableTable/EditableTable";
import VersionsList from "../components/VersionsList/VersionsList";

interface Props {
    properties: Property[];
    getData: () => void;
    editData: (property: Property) => void;
    deleteData: (id: number) => void;
}

class PropertyList extends React.PureComponent<Props> {
    columns = [{
        title: 'ID',
        dataIndex: 'id',
        key: 'id',
        editable: false
    }, {
        title: 'Host',
        dataIndex: 'owner',
        key: 'owner',
    }, {
        title: 'Number Of Bedrooms',
        dataIndex: 'numberOfBedrooms',
        key: 'numberOfBedrooms',
    }, {
        title: 'Number Of Bathrooms',
        dataIndex: 'numberOfBathrooms',
        key: 'numberOfBathrooms',
    }, {
        title: 'Airbnb ID',
        dataIndex: 'airbnbId',
        key: 'airbnbId',
    }, {
        title: 'Income Generated',
        dataIndex: 'incomeGenerated',
        key: 'incomeGenerated',
    }];

    componentDidMount() {
        this.props.getData();
    }

    expandedRowRender = (row: Property) => <VersionsList versions={row.versions} />;

    render() {
        return <EditableTable
            data={this.props.properties}
            columns={this.columns}
            onSave={this.props.editData}
            onDelete={this.props.deleteData}
            expandedRowRender={this.expandedRowRender}
        />;
    }
}

export default connect(
    (state: AppState) => ({ properties: state.property.properties}),
    {
        getData,
        editData,
        deleteData
    }
)(PropertyList);