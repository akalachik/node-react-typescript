import { PropertyState } from './property/interfaces';

export interface AppState {
    property: PropertyState;
}
