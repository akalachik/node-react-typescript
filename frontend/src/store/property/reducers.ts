import { Action } from 'redux';
import { PropertyState } from './interfaces';
import { GET_DATA_SUCCESS, GetDataSuccess } from './actions';

const defaultState: PropertyState = {
    properties: [],
};

export default (state = defaultState, action: Action): PropertyState => {
    switch (action.type) {
        case GET_DATA_SUCCESS:
            return { ...state, properties: (action as GetDataSuccess).properties };
            break;

        default:
            return state;
            break;
    }
};
