import { Property } from './interfaces';
import { Action, Dispatch } from 'redux';

export const GET_DATA_SUCCESS = 'property/getDataSuccess';

export interface GetDataSuccess extends Action {
    properties: Property[];
}

export const getDataSuccess = (properties: Property[]): GetDataSuccess => ({
    type: GET_DATA_SUCCESS,
    properties,
});

export const getData = () => {
    return async (dispatch: Dispatch) => {
        const response = await fetch('/api/property');
        const properties = await response.json();

        return dispatch(getDataSuccess(properties));
    };
};

export const editData = (data: Property) => {
    return async (dispatch: Dispatch) => {
        const response = await fetch(`/api/property/${data.id}`, {
            method: 'PUT',
            body: JSON.stringify({ ...data, id: undefined }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        });

        const result = await response.json();

        if (result) {
            dispatch(getData() as any);
        }
    };
};

export const deleteData = (id: number) => {
    return async (dispatch: Dispatch) => {
        const response = await fetch(`/api/property/${id}`, {
            method: 'DELETE',
        });

        const result = await response.json();

        if (result) {
            dispatch(getData() as any);
        }
    };
};
