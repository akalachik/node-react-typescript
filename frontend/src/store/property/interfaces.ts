export interface PropertyState {
    properties: Property[];
}

export interface Address {
    line1: string;
    line2?: string;
    line3?: string;
    line4?: string;
    postCode: string;
    city: string;
    country: string;
}

export interface PropertyVersion {
    owner: string;
    address: Address;
    numberOfBedrooms: number;
    numberOfBathrooms: number;
    airbnbId: number;
    incomeGenerated: number;
    createdAt: Date;
}

export interface Property {
    id: number;
    owner: string;
    address: Address;
    numberOfBedrooms: number;
    numberOfBathrooms: number;
    airbnbId: number;
    incomeGenerated: number;
    versions: PropertyVersion[];
}
