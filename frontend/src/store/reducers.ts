import { combineReducers } from 'redux';

import PropertyReducer from './property/reducers';

export default combineReducers({
    property: PropertyReducer,
});
