export interface Address {
    line1: string;
    line2?: string;
    line3?: string;
    line4?: string;
    postCode: string;
    city: string;
    country: string;
}
