import { Address } from '../address';

export interface CreatePropertyRequest {
    owner: string;
    address: Address;
    numberOfBedrooms: number;
    numberOfBathrooms: number;
    airbnbId: number;
    incomeGenerated: number;
}
