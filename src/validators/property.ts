import joi, { ValidationResult } from 'joi';
import { CreatePropertyRequest } from '../interfaces/requests/CreateProperty';

const addressSchema = joi.object().keys({
    line1: joi.string().required(),
    line2: joi.string().optional().allow(['', null]),
    line3: joi.string().optional().allow(['', null]),
    line4: joi.string().required(),
    postCode: joi.string().required(),
    city: joi.string().required(),
    country: joi.string().required(),
});

const propertySchema = joi.object().keys({
    owner: joi.string().required(),
    address: addressSchema,
    numberOfBedrooms: joi.number().required().greater(-1),
    numberOfBathrooms: joi.number().required().positive().greater(0),
    airbnbId: joi.number().required().positive().greater(0),
    incomeGenerated: joi.number().required().positive().greater(0),
});

export function validToCreate(request: CreatePropertyRequest): ValidationResult<CreatePropertyRequest> {
    return joi.validate(request, propertySchema);
}
