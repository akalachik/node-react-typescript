import { Entity, Column, BaseEntity, CreateDateColumn, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Address } from '../interfaces/address';
import { Property } from './property';

@Entity()
export class PropertyVersion extends BaseEntity {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public owner: string;

    @Column('json')
    public address: Address;

    @Column()
    public numberOfBedrooms: number;

    @Column()
    public numberOfBathrooms: number;

    @Column()
    public airbnbId: number;

    @Column('double')
    public incomeGenerated: number;

    @CreateDateColumn()
    public createdAt: Date;

    @ManyToOne((type: string) => Property, (property: Property) => property.versions, { onDelete: 'CASCADE' })
    public property: Property;
}
