import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, OneToMany} from 'typeorm';
import { Address } from '../interfaces/address';
import { PropertyVersion } from './propertyVersion';

@Entity()
export class Property extends BaseEntity {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public owner: string;

    @Column('json')
    public address: Address;

    @Column()
    public numberOfBedrooms: number;

    @Column()
    public numberOfBathrooms: number;

    @Column()
    public airbnbId: number;

    @Column('double')
    public incomeGenerated: number;

    @OneToMany((type: string) => PropertyVersion, (propertyVersion: PropertyVersion) => propertyVersion.property)
    public versions: PropertyVersion[];
}
