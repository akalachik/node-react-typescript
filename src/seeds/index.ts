const data = require('./data.json');

import { create } from '../services/property';

export async function initSeeds() {
    for (let i = 0; i < (data as any[]).length; i++) {
        await create(data[i]);
    }
}
