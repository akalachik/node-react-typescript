import { Property } from '../models/property';
import { PropertyVersion } from '../models/propertyVersion';
import { NotFoundError } from '../errors/NotFound';
import { CreatePropertyRequest } from '../interfaces/requests/CreateProperty';
import { validToCreate } from '../validators/property';
import { checkPropertyExists } from './airbnb';

export function list(relations: string[] = []): Promise<Property[]> {
    return Property.find<Property>({ relations });
}

export async function get(id: number, relations: string[] = [], validate = false): Promise<Property> {
    validate && checkId(id);

    const property = await Property.findOne<Property>(id, { relations });

    if (!property) {
        throw new NotFoundError();
    }

    return property;
}

export async function remove(id: number, validate = false): Promise<boolean> {
    validate && checkId(id);

    try {
        await Property.delete(id);

        return true;
    } catch (error) {
        console.log(error);
        return false;
    }
}

export function create(request: CreatePropertyRequest, validate = false): Promise<Property> {
    if (validate) {
        const validationResult = validToCreate(request);

        if (validationResult.error !== null) {
            throw new Error(validationResult.error.annotate());
        }

        if (!checkPropertyExists(request.airbnbId)) {
            throw new Error('AirbnbID is not right!');
        }
    }

    return Property.create<Property>(request).save();
}

export async function update(id: number, request: CreatePropertyRequest, validate = false): Promise<boolean> {
    if (validate) {
        const validationResult = validToCreate(request);

        if (validationResult.error !== null) {
            throw new Error(validationResult.error.annotate());
        }

        if (!checkPropertyExists(request.airbnbId)) {
            throw new Error('AirbnbID is not right!');
        }
    }

    try {
        const property = await get(id);

        const propertyJson = {...property};

        const result = await Property.update<Property>( id, request );

        if (!result.raw.changedRows) {
            return false;
        }

        delete propertyJson.id;
        delete propertyJson.versions;

        const version = PropertyVersion.create(propertyJson);
        version.property = property;

        await version.save();
    } catch (error) {
        return false;
    }

    return true;
}

function checkId(id: number) {
    if (!+id || +id <= 0) {
        throw new Error('Property id should be integer more than 0');
    }
}
