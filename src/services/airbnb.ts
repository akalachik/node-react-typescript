import axios from 'axios';
import CachingService from './caching';

export async function checkPropertyExists(airbnbId: number) {
    if (!+airbnbId) {
        return false;
    }

    const cachingKey = `airbnb_property_${airbnbId}`;

    const cachedValue = CachingService.get(cachingKey);

    if (typeof cachedValue !== 'undefined') {
        return cachedValue;
    }

    const url = `${process.env.BASE_AIRBNB_URL}/${airbnbId}`;

    try {
        const response = await axios.get(url, { maxRedirects: 0 });

        CachingService.set(cachingKey, response.status === 200);
    } catch (error) {
        CachingService.set(cachingKey, false);
    }

    return CachingService.get(cachingKey);
}
