// @todo In real life I prefer to use something like Redis as cache storage,
// but that in-memory implementation should be fine as for test task

export default class CachingService {
    public static set(key: string, value: any) {
        CachingService.storage[key] = value;
    }

    public static get(key: string) {
        return CachingService.storage[key];
    }

    private static storage: { [key: string]: any } = {};
}
