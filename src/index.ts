import 'reflect-metadata';
import express from 'express';
import dotenv from 'dotenv';
import bodyParser from 'body-parser';
import { Express } from 'express';
import { createConnection } from 'typeorm';

import PropertyRouter from './routes/property';
import { initSeeds } from './seeds/index';

dotenv.config();

const app: Express = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/api/property', PropertyRouter);

app.use('/', express.static(__dirname + '/../frontend'));

app.listen(process.env.APP_PORT, async () => {
    console.log(`Server started at port ${process.env.APP_PORT}`);

    try {
        const extension = process.env.NODE_ENV === 'production' ? 'js' : 'ts';

        await createConnection({
            type: 'mysql',
            host: process.env.DB_HOST,
            port: +process.env.DB_PORT,
            username: process.env.DB_USER,
            password: process.env.DB_PASSWORD,
            database: process.env.DB_NAME,
            entities: [
                __dirname + `/models/*.${extension}`,
            ],
            synchronize: process.env.NODE_ENV === 'development', // @todo - do migrations for production
        });

        console.log('Database connected!');

        if (process.env.NODE_ENV === 'development') {
            await initSeeds();

            console.log('Added seeds data!');
        }
    } catch (error) {
        console.log('Database connection failed', error.message);

        process.exit(0);
    }
});
