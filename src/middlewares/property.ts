import { Request, Response, NextFunction } from 'express';
import { get } from '../services/property';
import { NotFoundError } from '../errors/NotFound';
import { CreatePropertyRequest } from '../interfaces/requests/CreateProperty';
import { validToCreate } from '../validators/property';
import { checkPropertyExists } from '../services/airbnb';

export async function validatePropertyRequest(req: Request, res: Response, next: NextFunction) {
    const property: CreatePropertyRequest = req.body;

    const validationResults = validToCreate(property);

    if (validationResults.error === null) {
        if (! await checkPropertyExists(property.airbnbId)) {
            return res.status(400).json('AirbnbID is not right!');
        }

        return next();
    }

    return res.status(400).json(validationResults.error.details);
}

export async function validatePropertyExists(req: Request, res: Response, next: NextFunction) {
    const id = +req.params.id;

    if (!id) {
        return res.status(400).json({ error: 'Invalid request' });
    }

    try {
        res.locals.property = await get(id);
    } catch (error) {
        if (error instanceof NotFoundError) {
            return res.status(404).json('Not found');
        }

        return res.status(500).json(error);
    }

    next();
}
