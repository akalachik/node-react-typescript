import { Request, Response } from 'express';
import { list, get, remove, create, update } from '../services/property';

export default class PropertyController {
    public static async list(req: Request, res: Response): Promise<void> {
        try {
            const properties = await list(['versions']);

            res.json(properties);
        } catch (error) {
            res.status(500).json(error);
        }
    }

    public static async get(req: Request, res: Response): Promise<any> {
        try {
            const property = await get(req.params.id, ['versions']);

            res.json(property);
        } catch (error) {
            res.status(500).json(error);
        }
    }

    public static async create(req: Request, res: Response): Promise<void> {
        try {
            const property = await create(req.body);

            res.json(property);
        } catch (error) {
            res.status(400).json(error);
        }
    }

    public static async remove(req: Request, res: Response): Promise<void> {
        const result = await remove(req.params.id);

        res.json(result);
    }

    public static async update(req: Request, res: Response): Promise<void> {
        try {
            const result = await update(req.params.id, req.body);

            res.json(result);
        } catch (error) {
            res.status(400).json(error);
        }
    }
}
