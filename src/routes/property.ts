import { Router } from 'express';
import express from 'express';

import PropertyController from '../controllers/property';
import { validatePropertyRequest, validatePropertyExists } from '../middlewares/property';

const router: Router = express.Router();

router.get('/', PropertyController.list);
router.get('/:id', validatePropertyExists, PropertyController.get);
router.post('/', validatePropertyRequest, PropertyController.create);
router.delete('/:id', validatePropertyExists, PropertyController.remove);
router.put('/:id', validatePropertyExists, validatePropertyRequest, PropertyController.update);
router.patch('/:id', validatePropertyExists, validatePropertyRequest, PropertyController.update);

export default router;
