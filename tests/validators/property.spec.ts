import { expect } from 'chai';
import { CreatePropertyRequest } from '../../src/interfaces/requests/CreateProperty';
import { validToCreate } from '../../src/validators/property';

const validRequest: CreatePropertyRequest = {
    owner: 'elaine',
    address: {
        line1: '4',
        line2: '332b',
        line3: 'Hello World',
        line4: 'Goswell Road',
        postCode: 'EC1V 7LQ',
        city: 'London',
        country: 'U.K.',
    },
    airbnbId: 12220057,
    numberOfBedrooms: 2,
    numberOfBathrooms: 2,
    incomeGenerated: 1200,
};

describe('Property validator', () => {
    it('should have error as null if request is valid', () => {
        const result = validToCreate(validRequest);

        expect(result.error).to.be.eq(null);
    });

    it('should have error if owner is empty', () => {
        const result = validToCreate({ ...validRequest, owner: '' });

        expect(result.error).to.exist;
        expect(result.error.details).to.exist;
        expect(result.error.details[0].path[0]).to.be.eq('owner');
    });

    it('should have error if address line1 is empty', () => {
        const result = validToCreate({ ...validRequest, address: {...validRequest.address, line1: ''} });

        expect(result.error).to.exist;
        expect(result.error.details).to.exist;
        expect(result.error.details[0].path[0]).to.be.eq('address');
    });

    it('should not have error if address line2 is empty', () => {
        const result = validToCreate({ ...validRequest, address: {...validRequest.address, line2: null} });

        expect(result.error).to.be.eq(null);
    });

    it('should not have error if address line3 is empty', () => {
        const result = validToCreate({ ...validRequest, address: {...validRequest.address, line3: ''} });

        expect(result.error).to.be.eq(null);
    });

    it('should have error if address line4 is empty', () => {
        const result = validToCreate({ ...validRequest, address: {...validRequest.address, line4: ''} });

        expect(result.error).to.exist;
        expect(result.error.details).to.exist;
        expect(result.error.details[0].path[0]).to.be.eq('address');
    });

    it('should have error if address postCode is empty', () => {
        const result = validToCreate({ ...validRequest, address: {...validRequest.address, postCode: ''} });

        expect(result.error).to.exist;
        expect(result.error.details).to.exist;
        expect(result.error.details[0].path[0]).to.be.eq('address');
    });

    it('should have error if address city is empty', () => {
        const result = validToCreate({ ...validRequest, address: {...validRequest.address, city: ''} });

        expect(result.error).to.exist;
        expect(result.error.details).to.exist;
        expect(result.error.details[0].path[0]).to.be.eq('address');
    });

    it('should have error if address country is empty', () => {
        const result = validToCreate({ ...validRequest, address: {...validRequest.address, country: ''} });

        expect(result.error).to.exist;
        expect(result.error.details).to.exist;
        expect(result.error.details[0].path[0]).to.be.eq('address');
    });

    it('should have error if airbnbId is empty', () => {
        const result = validToCreate({ ...validRequest, airbnbId: 0 });

        expect(result.error).to.exist;
        expect(result.error.details).to.exist;
        expect(result.error.details[0].path[0]).to.be.eq('airbnbId');
    });

    it('should have error if airbnbId is less than 0', () => {
        const result = validToCreate({ ...validRequest, airbnbId: -100 });

        expect(result.error).to.exist;
        expect(result.error.details).to.exist;
        expect(result.error.details[0].path[0]).to.be.eq('airbnbId');
    });

    it('should have error if airbnbId is not number', () => {
        const result = validToCreate({ ...validRequest, airbnbId: 'qwerty' as any });

        expect(result.error).to.exist;
        expect(result.error.details).to.exist;
        expect(result.error.details[0].path[0]).to.be.eq('airbnbId');
    });

    it('should have error if numberOfBedrooms is null', () => {
        const result = validToCreate({ ...validRequest, numberOfBedrooms: null });

        expect(result.error).to.exist;
        expect(result.error.details).to.exist;
        expect(result.error.details[0].path[0]).to.be.eq('numberOfBedrooms');
    });

    it('should have error if numberOfBedrooms is less than 0', () => {
        const result = validToCreate({ ...validRequest, numberOfBedrooms: -100 });

        expect(result.error).to.exist;
        expect(result.error.details).to.exist;
        expect(result.error.details[0].path[0]).to.be.eq('numberOfBedrooms');
    });

    it('should have error if numberOfBedrooms is not number', () => {
        const result = validToCreate({ ...validRequest, numberOfBedrooms: 'qwerty' as any });

        expect(result.error).to.exist;
        expect(result.error.details).to.exist;
        expect(result.error.details[0].path[0]).to.be.eq('numberOfBedrooms');
    });

    it('should not have error if numberOfBedrooms is 0', () => {
        const result = validToCreate({ ...validRequest, numberOfBedrooms: 0 });

        expect(result.error).to.be.eq(null);
    });

    it('should have error if numberOfBathrooms is null', () => {
        const result = validToCreate({ ...validRequest, numberOfBathrooms: null });

        expect(result.error).to.exist;
        expect(result.error.details).to.exist;
        expect(result.error.details[0].path[0]).to.be.eq('numberOfBathrooms');
    });

    it('should have error if numberOfBathrooms is less than 0', () => {
        const result = validToCreate({ ...validRequest, numberOfBathrooms: -100 });

        expect(result.error).to.exist;
        expect(result.error.details).to.exist;
        expect(result.error.details[0].path[0]).to.be.eq('numberOfBathrooms');
    });

    it('should have error if numberOfBathrooms is not number', () => {
        const result = validToCreate({ ...validRequest, numberOfBathrooms: 'qwerty' as any });

        expect(result.error).to.exist;
        expect(result.error.details).to.exist;
        expect(result.error.details[0].path[0]).to.be.eq('numberOfBathrooms');
    });

    it('should have error if numberOfBathrooms is 0', () => {
        const result = validToCreate({ ...validRequest, numberOfBathrooms: 0 });

        expect(result.error).to.exist;
        expect(result.error.details).to.exist;
        expect(result.error.details[0].path[0]).to.be.eq('numberOfBathrooms');
    });

    it('should have error if incomeGenerated is null', () => {
        const result = validToCreate({ ...validRequest, incomeGenerated: null });

        expect(result.error).to.exist;
        expect(result.error.details).to.exist;
        expect(result.error.details[0].path[0]).to.be.eq('incomeGenerated');
    });

    it('should have error if incomeGenerated is less than 0', () => {
        const result = validToCreate({ ...validRequest, incomeGenerated: -100 });

        expect(result.error).to.exist;
        expect(result.error.details).to.exist;
        expect(result.error.details[0].path[0]).to.be.eq('incomeGenerated');
    });

    it('should have error if incomeGenerated is not number', () => {
        const result = validToCreate({ ...validRequest, incomeGenerated: 'qwerty' as any });

        expect(result.error).to.exist;
        expect(result.error.details).to.exist;
        expect(result.error.details[0].path[0]).to.be.eq('incomeGenerated');
    });

    it('should have error if incomeGenerated is 0', () => {
        const result = validToCreate({ ...validRequest, incomeGenerated: 0 });

        expect(result.error).to.exist;
        expect(result.error.details).to.exist;
        expect(result.error.details[0].path[0]).to.be.eq('incomeGenerated');
    });
});
