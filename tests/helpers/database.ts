import { Connection, createConnection } from 'typeorm';

import dotenv from 'dotenv';
import * as path from 'path';
import 'reflect-metadata';

dotenv.config({ path: path.resolve(process.cwd(), '.env.test') });

let connection: Connection;

export async function establishDatabaseConnection() {
    if (connection) {
        return connection;
    }
    connection = await createConnection({
        type: 'mysql',
        host: process.env.DB_HOST,
        port: +process.env.DB_PORT,
        username: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
        entities: [
            __dirname + `/../../src/models/*.ts`,
        ],
    });

    await connection.synchronize(true);

    return connection;
}

export async function closeConnection() {
    connection && await connection.close();

    connection = null;
}
