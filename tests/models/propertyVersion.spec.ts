import { expect } from 'chai';
import { CreatePropertyRequest } from '../../src/interfaces/requests/CreateProperty';
import { Property } from '../../src/models/property';
import { PropertyVersion } from '../../src/models/propertyVersion';
import { closeConnection, establishDatabaseConnection } from '../helpers/database';

describe('PropertyVersion model', () => {
    const validPropertyRequest: CreatePropertyRequest = {
        owner: 'elaine',
        address: {
            line1: '4',
            line2: '332b',
            line3: 'Hello World',
            line4: 'Goswell Road',
            postCode: 'EC1V 7LQ',
            city: 'London',
            country: 'U.K.',
        },
        airbnbId: 12220057,
        numberOfBedrooms: 2,
        numberOfBathrooms: 2,
        incomeGenerated: 1200,
    };

    const validPropertyVersionRequest: CreatePropertyRequest = {
        owner: 'elaine test',
        address: {
            line1: '4',
            line2: '332b',
            line3: 'Hello World',
            line4: 'Goswell Road',
            postCode: 'EC1V 7LQ',
            city: 'London',
            country: 'U.K.',
        },
        airbnbId: 1222005,
        numberOfBedrooms: 3,
        numberOfBathrooms: 2,
        incomeGenerated: 1500,
    };

    let property: Property;

    before(async () => {
       await establishDatabaseConnection();

       property = await Property.create<Property>(validPropertyRequest).save();
    });

    after(async () => {
       await closeConnection();
    });

    it('should create property version model with all properties', async () => {
        const version = await PropertyVersion.create<PropertyVersion>(validPropertyVersionRequest);

        version.property = property;

        await version.save();

        expect(version.id).to.be.gt(0);
        expect(version.property.id).to.be.eq(property.id);
        expect(version.owner).to.be.eq(validPropertyVersionRequest.owner);
        expect(version.address.line1).to.be.eq(validPropertyVersionRequest.address.line1);
        expect(version.address.line2).to.be.eq(validPropertyVersionRequest.address.line2);
        expect(version.address.line3).to.be.eq(validPropertyVersionRequest.address.line3);
        expect(version.address.line4).to.be.eq(validPropertyVersionRequest.address.line4);
        expect(version.address.postCode).to.be.eq(validPropertyVersionRequest.address.postCode);
        expect(version.address.city).to.be.eq(validPropertyVersionRequest.address.city);
        expect(version.address.country).to.be.eq(validPropertyVersionRequest.address.country);
        expect(version.airbnbId).to.be.eq(validPropertyVersionRequest.airbnbId);
        expect(version.numberOfBedrooms).to.be.eq(validPropertyVersionRequest.numberOfBedrooms);
        expect(version.numberOfBathrooms).to.be.eq(validPropertyVersionRequest.numberOfBathrooms);
        expect(version.incomeGenerated).to.be.eq(validPropertyVersionRequest.incomeGenerated);
    });
});
