import { expect } from 'chai';
import { CreatePropertyRequest } from '../../src/interfaces/requests/CreateProperty';
import { Property } from '../../src/models/property';
import { closeConnection, establishDatabaseConnection } from '../helpers/database';

describe('Property model', () => {
    const validRequest: CreatePropertyRequest = {
        owner: 'elaine',
        address: {
            line1: '4',
            line2: '332b',
            line3: 'Hello World',
            line4: 'Goswell Road',
            postCode: 'EC1V 7LQ',
            city: 'London',
            country: 'U.K.',
        },
        airbnbId: 12220057,
        numberOfBedrooms: 2,
        numberOfBathrooms: 2,
        incomeGenerated: 1200,
    };

    before(async () => {
       await establishDatabaseConnection();
    });

    after(async () => {
       await closeConnection();
    });

    it('should create property model with all properties', async () => {
        const property = await Property.create<Property>(validRequest).save();

        expect(property.id).to.be.gt(0);
        expect(property.owner).to.be.eq(validRequest.owner);
        expect(property.address.line1).to.be.eq(validRequest.address.line1);
        expect(property.address.line2).to.be.eq(validRequest.address.line2);
        expect(property.address.line3).to.be.eq(validRequest.address.line3);
        expect(property.address.line4).to.be.eq(validRequest.address.line4);
        expect(property.address.postCode).to.be.eq(validRequest.address.postCode);
        expect(property.address.city).to.be.eq(validRequest.address.city);
        expect(property.address.country).to.be.eq(validRequest.address.country);
        expect(property.airbnbId).to.be.eq(validRequest.airbnbId);
        expect(property.numberOfBedrooms).to.be.eq(validRequest.numberOfBedrooms);
        expect(property.numberOfBathrooms).to.be.eq(validRequest.numberOfBathrooms);
        expect(property.incomeGenerated).to.be.eq(validRequest.incomeGenerated);
    });

    it('should fail with validation error on owner', async () => {
        try {
            await Property.create<Property>({ ...validRequest, owner: null }).save();

            return Promise.reject('Should not create property without owner!');
        } catch (error) {
            expect(error).to.exist;
        }
    });

    it('should fail with validation error on airbnbId', async () => {
        try {
            await Property.create<Property>({ ...validRequest, airbnbId: null }).save();

            return Promise.reject('Should not create property without airbnbId!');
        } catch (error) {
            expect(error).to.exist;
        }
    });

    it('should fail with validation error on numberOfBedrooms', async () => {
        try {
            await Property.create<Property>({ ...validRequest, numberOfBedrooms: null }).save();

            return Promise.reject('Should not create property without numberOfBedrooms!');
        } catch (error) {
            expect(error).to.exist;
        }
    });

    it('should fail with validation error on numberOfBathrooms', async () => {
        try {
            await Property.create<Property>({ ...validRequest, numberOfBedrooms: null }).save();

            return Promise.reject('Should not create property without numberOfBathrooms!');
        } catch (error) {
            expect(error).to.exist;
        }
    });

    it('should fail with validation error on incomeGenerated', async () => {
        try {
            await Property.create<Property>({ ...validRequest, incomeGenerated: null }).save();

            return Promise.reject('Should not create property without incomeGenerated!');
        } catch (error) {
            expect(error).to.exist;
        }
    });

    it('should fail with validation error on address', async () => {
        try {
            await Property.create<Property>(
                { ...validRequest, address: null },
            ).save();

            return Promise.reject('Should not create property without address!');
        } catch (error) {
            expect(error).to.exist;
        }
    });
});
