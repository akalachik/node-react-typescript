import sinon from 'sinon';
import { SinonStubbedInstance } from 'sinon';
import { expect } from 'chai';
import axios from 'axios';
import { checkPropertyExists } from '../../src/services/airbnb';

describe('Airbnb service', () => {
    let stub: SinonStubbedInstance<any>;

    const goodAirbnbId = 2354700;
    const badAirbnbId = 242424242424​;

    before(() => {
       stub = sinon.stub(axios, 'get');
       stub.callsFake((url: string) => {
           const parts = url.split('/');

           const id = parts[parts.length - 1];

           if (+id === +goodAirbnbId) {
               return {
                   status: 200,
               };
           }

           return { status: 301 };
       });
    });

    after(() => {
       stub.restore();
    });

    it('should return true with good airbnb id', async () => {
       const result = await checkPropertyExists(goodAirbnbId);

       expect(result).to.be.eq(true);
   });

    it('should return false with bad airbnb id', async () => {
        const result = await checkPropertyExists(badAirbnbId);

        expect(result).to.be.eq(false);
    });

    it('should return false with wrong airbnb id', async () => {
        const result = await checkPropertyExists('hello' as any);

        expect(result).to.be.eq(false);
    });
});
