import { expect } from 'chai';
import CachingService from '../../src/services/caching';

describe('Caching service', () => {
    const cachingKey = 'hello_world_key';
    const cachedValue = 100500;

    it('should store data to cache storage', (done) => {
        try {
            CachingService.set(cachingKey, cachedValue);

            done();
        } catch (error) {
            done(error);
        }
    });

    it('should get data from cache storage', (done) => {
        try {
            const value = CachingService.get(cachingKey);

            expect(value).to.be.eq(cachedValue);

            done();
        } catch (error) {
            done(error);
        }
    });
    it('should rewrite data and get new one from cache storage', (done) => {
        const newValue = 'hello';

        try {
            CachingService.set(cachingKey, newValue);
            const value = CachingService.get(cachingKey);

            expect(value).to.be.eq(newValue);

            done();
        } catch (error) {
            done(error);
        }
    });
});
