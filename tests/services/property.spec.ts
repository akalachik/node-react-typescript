import { expect } from 'chai';
import { list, get, remove, create, update } from '../../src/services/property';
import { closeConnection, establishDatabaseConnection } from '../helpers/database';
import { Property } from '../../src/models/property';
import { CreatePropertyRequest } from '../../src/interfaces/requests/CreateProperty';

describe('Property Service', () => {
    const data: CreatePropertyRequest[] = [
        {
            owner: 'carlos',
            address: {
                line1: 'Flat 5',
                line4: '7 Westbourne Terrace',
                postCode: 'W2 3UL',
                city: 'London',
                country: 'U.K.',
            },
            airbnbId: 3512500,
            numberOfBedrooms: 1,
            numberOfBathrooms: 1,
            incomeGenerated: 2000.34,
        },
        {
            owner: 'ankur',
            address: {
                line1: '4',
                line2: 'Tower Mansions',
                line3: 'Off Station road',
                line4: '86-87 Grange Road',
                postCode: 'SE1 3BW',
                city: 'London',
                country: 'U.K.',
            },
            airbnbId: 1334159,
            numberOfBedrooms: 3,
            numberOfBathrooms: 1,
            incomeGenerated: 10000,
        },
        {
            owner: 'elaine',
            address: {
                line1: '4',
                line2: '332b',
                line4: 'Goswell Road',
                postCode: 'EC1V 7LQ',
                city: 'London',
                country: 'U.K.',
            },
            airbnbId: 12220057,
            numberOfBedrooms: 2,
            numberOfBathrooms: 2,
            incomeGenerated: 1200,
        },
    ];

    let listOfModels: Property[];

    before(async () => {
       await establishDatabaseConnection();

       for (let i = 0; i < data.length; i++) {
           await Property.create(data[i]).save();
       }
   });

    after(async () => {
       await closeConnection();
   });

    it('should return proper list of data', async () => {
       listOfModels = await list();

       expect(listOfModels.length).to.be.eq(data.length);
       expect(listOfModels[0].airbnbId).to.be.eq(data[0].airbnbId);
   });

    it('should return proper model by id', async () => {
       const object = await get(listOfModels[0].id);

       expect(object.id).to.be.eq(listOfModels[0].id);
       expect(object.airbnbId).to.be.eq(listOfModels[0].airbnbId);
   });

    it('should return error if model does not exist', async () => {
      try {
          await get(100500);

          return Promise.reject('Get action should not return model if it does not exist');
      } catch (error) {
          expect(error).to.exist;
      }
   });

    it('should remove model', async () => {
       const listLength = listOfModels.length;

       const result = await remove(listOfModels[0].id);

       expect(result).to.be.eq(true);

       listOfModels = await list();

       expect(listLength).to.be.eq(listOfModels.length + 1);
   });

    it('should create model', async () => {
       const listLength = listOfModels.length;

       const model = await create(data[0]);

       expect(model.id).to.be.gt(0);

       listOfModels = await list();

       expect(listLength).to.be.eq(listOfModels.length - 1);
   });

    it('should update model and create new version', async () => {
       const newOwner = 'Alexey';

       const result = await update(listOfModels[0].id, { ...data[0], owner: newOwner });

       expect(result).to.be.eq(true);

       const updatedModel = await get(listOfModels[0].id, ['versions']);

       expect(updatedModel.owner).to.be.eq(newOwner);
       expect(updatedModel.versions.length).to.be.eq(1);
       expect(updatedModel.versions[0].owner).to.be.eq(listOfModels[0].owner);
   });

    it('should not update model', async () => {
        const result = await update(100500, data[1]);

        expect(result).to.be.eq(false);
    });
});
