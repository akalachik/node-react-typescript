# Hostmakers

Test task for the Hostmakers interview

### Prerequisites

You need to have:

* Yarn installed 
* Node > 8 
* Mysql > 5.6

### Stack
* Node.js / Express / Typescript
* Mysql / TypeORM
* Tests with Chai / Mocha / Sinon / NYC
* Git hooks with Husky
* TSLint
* React / Redux / Thunk / Typescript
* Webpack

### Installing


Copy ```.env.example``` to ```.env``` and ```.env.test``` and fill with proper configuration

Do ```yarn install``` in ```.``` and ```/frontend``` dirs.

Do ```yarn start-dev``` in root directory.

### Additional features

You can also do ```yarn lint``` to lint your code (it will be executed automatically on every commit).

You can also do ```yarn test``` to test your code (it will be executed automatically on every push).

You can aslo do ```yarn test-coverage``` to generate your test coverage.